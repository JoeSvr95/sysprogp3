#include <stdio.h>
#define SIZE 15
#define ITER 10

int validarNombre(char s[]);
int validarEdad(int);

int main(){
	float edades = 0.0;
	char maxName[SIZE] = {0};
	char maxApe[SIZE] = {0};
	char minName[SIZE] = {0};
	char minApe[SIZE] = {0};
	int maxEdad, minEdad;
	maxEdad = 0;
	minEdad = 130;
	for (int i = 0; i < ITER; i++){
		int cond;
		char name[SIZE] = {0};
		char ape[SIZE] = {0};
		printf("Persona %d\n", i+1);
		do{
			for (int j = 0; j < SIZE; j++)
				name[j] = 0;
			printf("Ingrese su nombre: ");
			scanf("%s", name);
			cond = validarNombre(name);
			if (cond == 0)
				printf("Nombre no válido\n");
		}while(cond == 0);
		do{
			for (int j = 0; j < SIZE; j++)
				ape[j] = 0;
			printf("Ingrese su apellido: ");
			scanf("%s", ape);
			cond =validarNombre(ape);
			if (cond == 0)
				printf("Apellido no válido\n");
		}while(cond == 0);
		do{
			int edad = -1;
			printf("Ingrese su edad: ");
			scanf("%d", &edad);
			cond = validarEdad(edad);
			if (cond == 0)
				printf("Edad no válida\n");
			else
				edades = edades + edad;
				if (edad > maxEdad){
					maxEdad = edad;
					for (int j = 0; j < SIZE; j++){
						maxName[j] = name[j];
						maxApe[j] = ape[j];
					}
				}else if(edad < minEdad){
					minEdad = edad;
					for (int j = 0; j < SIZE; j++){
						minName[j] = name[j];
						minApe[j] = ape[j];
					}
				}
		}while(cond == 0);
	}
	printf("====================================\n");
	printf("Promedio de edad = %.2f\n", edades/ITER);
	printf("Persona más vieja: %s %s, edad = %d\n", maxName, maxApe, maxEdad);
	printf("Persona más joven: %s %s, edad = %d\n", minName, minApe, minEdad);
}

int validarNombre(char name[]){
	if (name[0] >= 'A' && name[0] <= 'Z'){
		for (int i = 1; i < SIZE; i++){
			char c = name[i];
			if ((c < 'a' || c > 'z') && c != 0)
				return 0;
		}
		return 1;
	}else
		return 0;
}

int validarEdad(int edad){
	if (edad >= 0 && edad <= 130)
		return 1;
	else
		return 0;
}
